'''
Created on Sep 19, 2015

@author: yyf
'''
from wsgiref.simple_server import make_server
from webdemo import application

httpd = make_server('',8000,application)
print "Serving HTTP on port 8000.........."
httpd.serve_forever()