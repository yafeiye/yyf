#coding=utf8
'''
Created on Oct 25, 2015

@author: yyf
'''
import threading
from socket import *
PORT=1119#设置端口
BUFSIZE=1024#设置接收字节长度
def server():#server 端
    HOST='127.0.0.1'#设置端口
    ADDR=(HOST,PORT)
    sock=socket(AF_INET,SOCK_STREAM)
    sock.bind(ADDR)
    sock.listen(5)
    BOOL=True
    while BOOL:
        print'等待接入，侦听端口:%d' %(PORT)
        tcpClientSock,addr=sock.accept()
        print '接受连接，客户端地址：',addr
        while True:
            try:
                data=tcpClientSock.recv(BUFSIZE)
            except:
                tcpClientSock.close()
            if not data:
                BOOL=False
                break
            print '收到client信息：%s' %data
            print '.'*5
            if (data.isdigit()):
                mess=str(int(data)+1)
            else:
                mess=data
            if data.upper()=='QUIT':
                BOOL=False
                break
            tcpClientSock.send(mess)
    tcpClientSock.close()
    sock.close()
def client():
    HOST='127.0.0.1'
    ADDR=(HOST,PORT)
    cli=socket(AF_INET,SOCK_STREAM)
    cli.connect(ADDR)
    while True:
        data=raw_input('>')
        if not data:
            break
        cli.send(data)
        if data.upper()=='QUIT':
            break
        data1=cli.recv(BUFSIZE)
        if not data1:
            break
        print '收到server信息：%s' %data1
threads=[]
t1=threading.Thread(target=server)
threads.append(t1)
t2=threading.Thread(target=client)
threads.append(t2)
if __name__=='__main__':
    for t in threads:
        t.setDaemon(True)
        t.start()
    t.join()
    print 'over'
        
        
            

