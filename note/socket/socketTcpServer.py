#coding=utf-8
'''
Created on Oct 25, 2015

@author: yyf
'''
import socket
import thread

#
def server():
    sock.bind(ADDR)#将socket绑定到地址上
    while True:
        sock.listen(5)#监听请求 设置最多为5个
        print'等待接入，侦听端口:%d' % (PORT)
        tcpClientSock, addr=sock.accept()#等待用户请求一个链接  返回一个新的socket对象服务器通过它与client通信 和一个client端的ip地址
        print '接受连接，客户端地址：',addr 
        sockList.append(tcpClientSock)
        thread.start_new_thread(sockThread, (tcpClientSock,))  
def sockThread(tcpClientSock):
        while True:
            try:
                data=tcpClientSock.recv(BUFSIZE)#recv方法接收client端发送的信息
            except:
                tcpClientSock.close()           
            if data:
                print data
                for j in sockList:
                    if j!=tcpClientSock:#
                        try:
                            j.send(str(tcpClientSock)[-14:]+':'+data)#send方法发送信息到client端
                        except:
                            print 'someone is out'
        #tcpClientSock.close()            
if __name__=='__main__':
    HOST='127.0.0.1'
    PORT=1123  #设置侦听端口
    BUFSIZE=1024#设置接受数据的最大字节
    ADDR=(HOST, PORT)#地址为双元素元组
    sock=socket.socket()#创建socket对象
    sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    sockList=[]
    server()
