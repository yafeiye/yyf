Server

SSH
sudo apt-get install openssh-server
sudo nano /etc/ssh/sshd_config
注释掉#GSSAPIAuthentication yes
注释掉#GSSAPIDelegateCredentials no
sudo service ssh restart
chmod 600 authorized_keys

GIT
sudo apt-get install git
sudo git init --bare gitserver.git

注意uthorized_keys的权限  chmod 644 authorized_keys  这个也是要注意的。 
 
一般两个命令就好了 
 
命令：ssh-keygen -t dsa -P '' -f ~/.ssh/id_dsa 
 
此时用户目录的.ssh目录下生成id_dsa（私钥）、id_dsa.pub（公钥） 
 
命令：cat ~/.ssh/id_dsa.pub >> ~/.ssh/authorized_keys 
 
将公钥添加到authorized_keys中。 




