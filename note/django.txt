#django控制台创建工程
django-admin.py startproject zqxt_admin
进入 zqxt_admin 文件夹
django-admin.py startapp blog
进入包含有 manage.py 的文件夹
python manage.py syncdb
注意：Django 1.7及以上的版本需要用以下命令
python manage.py makemigrations
python manage.py migrate

#配置静态文件
setting中添加
STATICFILES_DIRS = (os.path.join(BASE_DIR, "common_static"),)
在工程目录下创建common_static文件夹  
调用静态文件  /static/...

#配置DATABASES
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '表名',
        'USER':'root',
        'PASSWORD':'123456',
        'HOST':'数据库地址',
        'PORT':'',
    }
}

#配置TEMPLATE
TEMPLATE_DIRS=(
    '/(applaction所在的绝对路径)/templates'
)

#配置url映射
url(r'^ACTION/$','APPNAME.views.ACTION',name='ACTION'),
url(r'^APPNAME/',include('APPNAME.urls')),

#django session
存放数据到session
request.session['name'] = name
从session中读取存放的数据
session.get('name', False)
从session中将数据删除
del request.session['name']
让session过期
request.session.set_expiry()
SESSION_SAVE_EVERY_REQUEST 如果True,django为每次request请求都保存session的内容，默认为False。
SESSION_EXPIRE_AT_BROWSER_CLOSE 如果True浏览器已关闭session就过期了，默认为False。
SESSION_COOKIE_AGE 设置SESSION的过期时间，单位是秒，默认是两周

#配置model
添加model类时需要再每个类中添加__unicode__方法
def __unicode__(self):
	return self.任意一个类的变量名

