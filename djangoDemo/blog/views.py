#coding=utf-8
from django.shortcuts import render
from blog.models import Article
from django.shortcuts import  render_to_response
from django import forms 
from django.http import HttpResponse
from .models import User
# Create your views here.
class UserForm(forms.Form):
    username = forms.CharField()
    headImg = forms.FileField()

def index(request):
    blog_list=Article.objects.all()
    if request.method == "POST":
        uf=UserForm(request.POST,request.FILES)
        if uf.is_valid():
            username=uf.cleaned_data['username']
            headImg=uf.cleaned_data['headImg']
            user=User()
            user.username =username
            user.headImg =headImg
            user.save()
            
            return HttpResponse("上传成功！")        
    else:
        uf = UserForm()
        return render_to_response('index.html',{'blog_list':blog_list,'uf':uf})    
    return render_to_response('index.html',{'blog_list':blog_list})