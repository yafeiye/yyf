#coding=utf-8
'''
Created on Oct 13, 2015

@author: yyf

moneyRun:以元为单位的随机分钱
需要接受的参数：参加moneyRun所有人的姓名的list 分钱的总钱数
返回值：返回一个键为姓名 值为钱的json
'''
import random
import json
class Money(object):
#listPerson=["张","郑","肖","奥","范","叶"]
    jsonMoney={}
    def moneyRun(self,listPerson,money):
        self.listPerson=listPerson
        self.money=money
        money=int(money)
        list=[0]*len(listPerson)#初始化每人的钱数
        peopleNumber=len(listPerson)#得到总人数
        self.run(list,listPerson,money,peopleNumber)#开始
        return self.jsonMoney
    def run(self,list,listPerson,money,peopleNumber):
        fenMoney=money/peopleNumber #取出每次每人可以分到的最大值
        money,ff=money-fenMoney*peopleNumber,0 #剩余的钱
        for i in range(0,peopleNumber): #循环分钱
            m=random.randint(0,int(fenMoney)) #random分钱
            list[i],ff=list[i]+m,ff+fenMoney-m #加上每次随机的钱 加上每次多出来的钱
        money=money+ff #每轮分钱后剩余的总钱数
        if money>peopleNumber: #钱数大于人数 继续分
            self.run(list,listPerson,money,peopleNumber)
        else:
            while money>0: #钱数小于人数就将剩余的钱按一块随机分给每人
                x=random.randint(0,peopleNumber-1)#随机得到谁分那一块钱
                list[x]=list[x]+1#将分到的钱加上
                money=money-1
            dictEnd= dict(zip(listPerson,list)) #将两个list合成dict 
            self.jsonMoney= json.dumps(dictEnd, ensure_ascii=False)#将dict转为json 处理编码
            #return jsonMoney#返回一个键为姓名 值为钱的json
            #moneyRun(listPerson,600.1)
#a=Money()
#listPerson=["张","郑","肖","奥","范","叶"]
#print a.moneyRun(listPerson,100)
#print c