'''
Created on Oct 14, 2015

@author: yyf
'''
from django.conf.urls import patterns,url
from fenMoney import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^login/$',views.login,name = 'login'),
    url(r'^ran/$',views.ran,name='ran'),
    )