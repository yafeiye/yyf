from django.db import models
#python manage.py syncdb
class Person(models.Model):
    name = models.CharField(max_length=30)
    age = models.IntegerField()
    
    def __unicode__(self):
        return self.name
