from django.shortcuts import render,render_to_response
from django.http import HttpResponse
from django import forms
from django.template import RequestContext
from .models import Person
class NameForm(forms.Form):
    name=forms.CharField(label='name',max_length=100)
    age=forms.CharField(label='age',max_length=100)
def addName(request):
    if request.method =='POST':
        form=NameForm(request.POST)
        if form.is_valid():
            name=form.cleaned_data['name']
            age=form.cleaned_data['age']
            Person.objects.create(name=name,age=age)
    else:
        form =NameForm()
    return render_to_response('index.html',{'form':form}, context_instance=RequestContext(request))