from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djangoDemo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'^$','mysite.views.index',name='home'),
    #url(r'^$','mysite.views.home',name='home'),
    #url(r'^$','tools.views.index',name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^online/',include('online.urls')),
    url(r'^fenMoney/',include('fenMoney.urls')),
    url(r'^index/$','blog.views.index',name='index'),
    #url(r'^ajax_dict/$','mysite.views.ajax_dict',name='ajax_dict'),
    url(r'^ajax_list/$', 'mysite.views.ajax_list', name='ajax-list'),
    url(r'^ajax_dict/$', 'mysite.views.ajax_dict', name='ajax-dict'),
    url(r'^home/$', 'mysite.views.home', name='home'),
    url(r'^add/$', 'mysite.views.add', name='add'),
    url(r'^addname/$','people.views.addName',name='addName'),
    url(r'^regist/$','online.views.regist',name='regist'),
    url(r'^login/$','online.views.login',name='login'),
)
