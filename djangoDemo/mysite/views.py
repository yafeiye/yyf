#coding=utf-8
from django.shortcuts import render
from django.http import  HttpResponse
import json
def ajax_list(request):
    a = range(100)
    return HttpResponse(json.dumps(a),content_type='application/json')
def ajax_dict(request):
    name_dict = {'twz': 'Love python and Django', 'zqxt': 'I am teaching Django'}
    return HttpResponse(json.dumps(name_dict), content_type='application/json')

def index(request):
    return HttpResponse('welcome to my django')
def home(request):
    string ='haha, what is your name?'
    return render(request,'home.html',{'string':string})
def add(request):
    a=request.GET['a']
    b=request.GET['b']
    a=int(a)
    b=int(b)
    return HttpResponse(str(a+b))
#def ajax_dict(request):
    #name_dict ={'哈啊':'this is ajax','哦哦':'python  is shit'}
    #return render(request,'home.html')
    #return HttpResponse(json.dumps(name_dict,ensure_ascii=False),content_type='application/json; charset=utf-8')
