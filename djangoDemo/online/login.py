#coding=utf-8
'''
Created on Oct 14, 2015

@author: yyf
login:登录 request GET取登录名 存session 跳转到room.html 并传值username
'''
from django.shortcuts import render,render_to_response
def login(request):
    username=request.GET['username']#从页面上拿到username
    #数据库操作
    request.session['username']=username#存session
    return render_to_response('room.html',{'username':username})#页面跳转
    