'''
Created on Sep 28, 2015

@author: yyf
'''
from django.conf.urls import patterns,url
from online import views

urlpatterns = patterns('',
    url(r'^$', views.login, name='login'),
    url(r'^login/$',views.login,name = 'login'),
    url(r'^regist/$',views.regist,name = 'regist'),
    url(r'^onlineHome/$',views.home,name = 'home'),
    url(r'^logout/$',views.logout,name = 'logout'),
    url(r'^index/$',views.index,name = 'index'),
    url(r'^user/$',views.user,name='user'),
)