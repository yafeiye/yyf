'''
Created on Oct 19, 2015

@author: yyf

接口: 微信卡券签名sha1
param: 调用addData传入签名的参数
return: sha1签名

'''
import hashlib
class WxCardSign(object):
    def __init__(self):
        self.__data=[]
    def addData(self,data):
        self.__data.append(str(data))
    def getSignature(self):
        self.__data.sort()
        strToSign =''.join(self.__data)
        print 'strToSign:' ,strToSign
        return hashlib.sha1(strToSign).hexdigest()

'''if __name__ =='__main__':
    signer =WxCardSign()
    signer.addData('yyf');
    signer.addData('123456');
    signer.addData('qwq'); 
    print signer.getSignature()'''