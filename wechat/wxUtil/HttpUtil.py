#coding=utf-8
'''
Created on Oct 19, 2015

@author: yyf

接口: http请求 get post(json)
param: get:请求的url  post:url json
return: 请求得到的参数
'''
import urllib2
import json
def httpRequest(url):
    request =urllib2.Request(url)
    requestData=urllib2.urlopen(request)
    request=requestData.read()
    return request
def httpResponse(url,values):
    data =json.dumps(values,ensure_ascii=False)
    request=urllib2.Request(url,data,{'Content-Type': 'application/json'})
    response =urllib2.urlopen(request)
    result =response.read()
    return result
