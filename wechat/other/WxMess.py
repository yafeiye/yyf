#coding=utf-8
'''
Created on Oct 22, 2015

@author: yyf
'''
from wxUtil import HttpUtil,GetAccessToken
#import json

#格式：2015-10-15
def getMessSendData(begin_data,end_data):
    jsonMess={ 
    "begin_date": begin_data, 
    "end_date": end_data
    }
    accessToken=GetAccessToken.accessToken()
    url='https://api.weixin.qq.com/datacube/getupstreammsg?access_token='+accessToken
    return HttpUtil.httpResponse(url,jsonMess)
#print getMessSendData('2015-10-15','2015-10-15')
print getMessSendData('2015-10-22','2015-10-23')