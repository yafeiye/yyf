#coding=utf8
'''
Created on Nov 4, 2015

@author: yyf
消息分析接口：内包含7个子接口  由于每个接口post数据一样 所以调用的接口按照传入的index区分
index=1：获取消息发送概况数据     最大时间跨度 7
index=2：获取消息分送分时数据     最大时间跨度 1
index=3：获取消息发送周数据       最大时间跨度 30
index=4：获取消息发送月数据       最大时间跨度 30
index=5：获取消息发送分布数据     最大时间跨度 15
index=6：获取消息发送分布周数据   最大时间跨度 30
index=7：获取消息发送分布月数据   最大时间跨度 30
调用格式 newsData('2015-11-02','2015-11-02',6)
传入参数： begin_date end_date index
返回 json 详情见微信文档
'''
from wxUtil import GetAccessToken,HttpUtil
def newsData(begin_date,end_date,index):
    accessToken=GetAccessToken.accessToken()#获取accessToken
    url = {
     1: 'https://api.weixin.qq.com/datacube/getupstreammsg?access_token=',
     2: 'https://api.weixin.qq.com/datacube/getupstreammsghour?access_token=',
     3: 'https://api.weixin.qq.com/datacube/getupstreammsgweek?access_token=',
     4: 'https://api.weixin.qq.com/datacube/getupstreammsgmonth?access_token=',
     5: 'https://api.weixin.qq.com/datacube/getupstreammsgdist?access_token=',
     6: 'https://api.weixin.qq.com/datacube/getupstreammsgdistweek?access_token=',
     7: 'https://api.weixin.qq.com/datacube/getupstreammsgdistmonth?access_token=',
    }[index]
    url+=accessToken#调用微信的接口地址
    values={ 
    "begin_date":begin_date,
    "end_date":end_date
    }
    return HttpUtil.httpResponse(url,values)
# print newsData('2015-10-01','2015-10-07',7)