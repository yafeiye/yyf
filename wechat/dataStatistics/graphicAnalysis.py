#coding=utf8
'''
Created on Nov 4, 2015

@author: yyf
图文分析接口：内包含6个子接口  由于每个接口post数据一样 所以调用的接口按照传入的index区分
index=1：获取图文群发每日数据  最大时间跨度 1
index=2：获取图文群发总数据    最大时间跨度 1
index=3：获取图文统计数据      最大时间跨度 3
index=4：获取图文统计分时数据   最大时间跨度 1
index=5：获取图文分享转发数据    最大时间跨度 7
index=6：获取图文分享转发分时数据  最大时间跨度 1
调用格式 graphicData('2015-11-02','2015-11-02',6)
传入参数： begin_date end_date index
返回 json 详情见微信文档
'''
from wxUtil import GetAccessToken,HttpUtil
def graphicData(begin_date,end_date,index):
    accessToken=GetAccessToken.accessToken()#获取accessToken
    url = {
     1: 'https://api.weixin.qq.com/datacube/getarticlesummary?access_token=',
     2: 'https://api.weixin.qq.com/datacube/getarticletotal?access_token=',
     3: 'https://api.weixin.qq.com/datacube/getuserread?access_token=',
     4: 'https://api.weixin.qq.com/datacube/getuserreadhour?access_token=',
     5: 'https://api.weixin.qq.com/datacube/getusershare?access_token=',
     6: 'https://api.weixin.qq.com/datacube/getusersharehour?access_token=',
    }[index]
    url+=accessToken#调用微信的接口地址
    values={ 
    "begin_date":begin_date,
    "end_date":end_date
    }
    return HttpUtil.httpResponse(url,values)
#print graphicData('2015-11-02','2015-11-02',6)