#coding=utf8
'''
Created on Nov 4, 2015

@author: yyf
获取接口分析数据      最大时间跨度 30
获取接口分析分时数据   最大时间跨度 1
传入参数：begin_date  end_date
返回参数：json 详情见微信文档
'''
from wxUtil import GetAccessToken,HttpUtil
def getInterfaceAnalysisData(begin_date,end_date):#获取接口分析数据
    accessToken=GetAccessToken.accessToken()#获取accessToken
    url='https://api.weixin.qq.com/datacube/getinterfacesummary?access_token='+accessToken#调用微信的接口地址
    values={ 
    "begin_date":begin_date,
    "end_date":end_date
    }
    return HttpUtil.httpResponse(url,values)
#print getInterfaceAnalysisData('2015-11-02','2015-11-03')#测试
def getInterfaceAnalysisByHourData(begin_date,end_date):#获取接口分析分时数据
    accessToken=GetAccessToken.accessToken()#获取accessToken
    url='https://api.weixin.qq.com/datacube/getinterfacesummaryhour?access_token='+accessToken#调用微信的接口地址
    values={ 
    "begin_date":begin_date,
    "end_date":end_date
    }
    return HttpUtil.httpResponse(url,values)
# print getInterfaceAnalysisData('2015-11-02','2015-11-03')#测试
