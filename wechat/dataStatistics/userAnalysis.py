#coding=utf8
'''
Created on Nov 4, 2015

@author: yyf
获取用户增减数据接口 最大时间跨度 7
获取累计用户数据接口  最大时间跨度 7
传入参数：begin_date  end_date
返回参数：json 详情见微信文档
'''
from wxUtil import GetAccessToken,HttpUtil
def userDecreaseData(begin_date,end_date):#获取用户增减数据  调用格式为userDecreaseData('2015-11-02','2015-11-03')
    accessToken=GetAccessToken.accessToken()#获取accessToken
    url='https://api.weixin.qq.com/datacube/getusersummary?access_token='+accessToken#调用微信的接口地址
    values={ 
    "begin_date":begin_date,
    "end_date":end_date
    }
    return HttpUtil.httpResponse(url,values)
#print userDecreaseData('2015-11-02','2015-11-03')#测试
def userAccumulationData(begin_date,end_date):#获取累计用户数据  调用格式为userAccumulationData('2015-11-02','2015-11-03')
    accessToken=GetAccessToken.accessToken()#获取accessToken
    url='https://api.weixin.qq.com/datacube/getusercumulate?access_token='+accessToken#调用微信的接口地址
    values={ 
    "begin_date":begin_date,
    "end_date":end_date
    }
    return HttpUtil.httpResponse(url,values)
#print userAccumulationData('2015-11-02','2015-11-03')#测试
    
    
    
    