#coding=utf8
'''
Created on Nov 5, 2015

@author: yyf
创建卡券的py程序 
类型：代金券（如果要生成其他类型的卡券改几个小参数即可）
使用方式：在main函数下写入卡券的参数信息 运行createWxCard.py
返回参数：errcode=0 即成功 errcode=1 则失败
'''
from wxUtil import GetAccessToken,HttpUtil
def createCard():
    accessToken=GetAccessToken.accessToken()#获取accessToken
    url='https://api.weixin.qq.com/card/create?access_token='+accessToken#调用微信的接口地址
    values={
        "card": {
            "card_type": "CASH",                             #类型：代金券
            "cash": {
                "base_info": {
                    "logo_url": "http://120.27.43.71:8080/store/file/logo/logo300.jpg",  #卡券logo`
                    "brand_name": brand_name,                #商户名
                    "code_type": "CODE_TYPE_QRCODE",         #code码类型
                    "title": title,                          #主标题
                    "sub_title": sub_title,                  #副标题
                    "color": color,                          #卡券颜色
                    "notice": notice,                        #卡券使用提醒
                    "service_phone": "400-110-4411",         #客服电话
                    "description": description,              #使用说明
                    "date_info": {
                        "type": "DATE_TYPE_FIX_TIME_RANGE",  #生效时间类型
                        "begin_timestamp": begin_timestamp,  #开始时间
                        "end_timestamp": end_timestamp       #结束时间
                    },
                    "sku": {
                        "quantity": quantity                 #卡券生成数量               
                    },
                    "get_limit": get_limit,                          #每人领取数量
                    "use_custom_code": False,                #是否自定义code
                    "can_share": can_share,                  #是否可以分享
                    "can_give_friend": can_give_friend,      #是否可以赠送
                    "bind_openid": False,                    #是否制定openid
                    "custom_url_name": custom_url_name,      #自定义链接名字
                    "custom_url": custom_url,                #自定义链接
                    "source": source                         #卡券下方字段
                },
                "least_cost": least_cost,                    #减免最低消费金额
                "reduce_cost": reduce_cost                   #减免金额（单位：分）          
            }
        }
    }
    print HttpUtil.httpResponse(url,values)
if __name__=='__main__':
############设置卡券基本参数################
    #商户名（<=12汉字）
    brand_name="十一平米(测试)"
    #主标题（<=9汉字）
    title="30元代金券（测试）"
    #副标题（<=18汉字）
    sub_title="十一平米（测试）"
    #卡券颜色（具体颜色见 http://mp.weixin.qq.com/wiki/8/b7e310e7943f7763450eced91fa793b0.html）
    color= "Color080"
    #使用说明（<=16汉字）
    notice="预约保养下单时即可使用"
    #卡券生效时间
    begin_timestamp= 1446310861
    #卡券失效时间
    end_timestamp=1448902861
    #使用说明（<=1024汉字）
    description="1.本券仅限武汉地区车主使用\n2.本券不兑换现金，不得同时享受其他保养优惠\n3.每次仅可使用一次代金券，不可叠加使用\n4.使用有效期：xxxx（测试）"
    #卡券生产数量
    quantity=10
    #每人领取数量
    get_limit=1
    #卡券是否可以分享 是否可以赠送               
    can_share,can_give_friend= False,True                       #
    #自定义链接名字
    custom_url_name="立即下单"
    #自定义链接
    custom_url="http://wechat.11pingm.com/choose_package.jsp"
    #卡券下方字段
    source="十一平米车养护" 
    #减免最低消费金额（单位：分）
    least_cost=10000
    #减免金额（单位：分）
    reduce_cost=3000
##################基本参数设置结束#########################  
    #run   返回 errcode=0 则为创建成功
    createCard()